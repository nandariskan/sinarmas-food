import 'package:sinarmas_food/features/meals/data/models/categories/categories_model.dart';
import 'package:sinarmas_food/features/meals/data/datasources/database.dart';

abstract class HomeState {}

class InitHomeState extends HomeState {}

class ShowLoadingHomeState extends HomeState {}

class ShowCategoriesDataHomeState extends HomeState {
  List<CategoryData> list;

  ShowCategoriesDataHomeState({this.list});
}

class ShowFavoriteFoodState extends HomeState {
  List<Food> list;

  ShowFavoriteFoodState({this.list});
}

class ShowErrorHomeState extends HomeState {
  String message;

  ShowErrorHomeState({this.message});
}