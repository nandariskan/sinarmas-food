import 'package:flutter/material.dart';
import 'package:sinarmas_food/features/meals/presentation/home/home_screen.dart';
import 'package:sinarmas_food/features/meals/presentation/utils/app_navigator.dart';
import 'package:splashscreen/splashscreen.dart';

class SinarSplash extends StatefulWidget {
  const SinarSplash({Key key}) : super(key: key);

  @override
  _SinarSplashState createState() => _SinarSplashState();
}

class _SinarSplashState extends State<SinarSplash> {

  @override
  void initState() {
    super.initState();
    Future.delayed(Duration(seconds: 2), () {
      AppNavigator().pushReplacement(context, child: HomeScreen());
    });
  }

  @override
  Widget build(BuildContext context) {
    return new SplashScreen(
      seconds: 4,
      image: Image.asset('assets/images/logo-01.png'),
        backgroundColor: Colors.white,
        styleTextUnderTheLoader: new TextStyle(),
        photoSize: 100.0,
        loaderColor: Colors.green
    );
  }
}
