import 'package:flutter/material.dart';
import 'package:sinarmas_food/features/meals/presentation/splash/splash_screen.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: new SinarSplash(),
      title: 'Sinar Food',
      theme: ThemeData(
        primaryColor: Color.fromRGBO(18, 37, 63, 1.0),
        accentColor: Color.fromRGBO(18, 37, 63, 1.0),
        appBarTheme: AppBarTheme(color: Color.fromRGBO(18, 37, 63, 1.0))
      ),
    );
  }
}
